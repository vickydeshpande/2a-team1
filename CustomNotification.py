from email.mime import image
from numpy import char
import schedule
from PIL import Image
import time
import os
from email.mime.text import MIMEText
import smtplib
from email.mime.multipart import MIMEMultipart
import tableauserverclient as TSC
from email.mime.image import MIMEImage


def task():

    tableau_auth = TSC.TableauAuth('akash.s.sharma@apisero.com',os.environ.get('tab_password'),'SteelProd' )
    tab_server = TSC.Server('https://prod-apnortheast-a.online.tableau.com/')

    viewID = [ '2d390d6c-8ca2-48a0-859c-a38db3b3ecc5' , '6f19b9bf-d9a7-4ed8-a3a9-6d8adc0e431f' , '1631de8a-4768-4f8e-bc4e-75ddd04bf011' , '9dc1a9de-ad54-406a-9e0b-17add8f5f00c']
    with tab_server.auth.sign_in(tableau_auth):
        tab_server.use_server_version()
        for i in viewID:
            view = tab_server.views.get_by_id(i)
            print(view.name)
            tab_server.views.populate_image(view)
            with open('./'+view.name+'.png', 'wb') as f:
                f.write(view.image)

            if( view.name=='Snowflake Query Utilization' ):    
                cropedImage = Image.open(r'./'+view.name+'.png')
                cropedImage = cropedImage.crop((0, 100, 800, 450))
                cropedImage.save('./'+view.name+'.png')

            if( view.name=='Performance Monitoring' ):    
                cropedImage = Image.open(r'./'+view.name+'.png')
                cropedImage = cropedImage.crop((200, 420, 800, 770))
                cropedImage.save('./'+view.name+'.png')

            if( view.name=='Slowest Running Queries' ):    
                cropedImage = Image.open(r'./'+view.name+'.png')
                cropedImage = cropedImage.crop((0, 460, 320, 770))
                cropedImage.save('./'+view.name+'.png')

            if( view.name== 'Compute Cost Overview' ):    
                cropedImage = Image.open(r'./'+view.name+'.png')
                cropedImage = cropedImage.crop((0, 80, 800, 420))
                cropedImage.save('./'+view.name+'.png')
            
            


    fromaddr = 'tableau.notification.one@gmail.com'
    toaddrs = [
            "abdul.m.hussain@kipi.bi",
            "sumit.r.bhatia@kipi.bi",
            "madhivanan.a.a@kipi.bi",
            "nipun.a.mahajan@kipi.bi",
            "jagdish.b.singh@kipi.bi",
            "mithlesh.m.kumar@kipi.bi",
            "amarnath.b.baranwal@kipi.bi",
            "kavish.a.kumar@kipi.bi",
            "sarthak.a.sharma@kipi.bi",
            "utkarsh.n.singh@kipi.bi",
            "apoorva.r.srivastva@kipi.bi",
            "pranit.j.jain@kipi.bi",
            "ayushi.s.sharma@kipi.bi",
            "madhur.m.rusia@kipi.bi",
            "nikhilesh.a.kashyap@kipi.bi",
            "siri.m.kademani@kipi.bi",
            "aayushi.m.pandey@kipi.bi",
            "chirag.s.jain@kipi.bi",
            "udit.p.saini@kipi.bi",
            "jaddu.s.sridevi@kipi.bi",
            "ankit.s.chauhan@kipi.bi",
            "jenish.r.karia@kipi.bi",
            "totli.c.manikanta@kipi.bi",
            "tarun.k.khetrapal@kipi.bi",
            "dhanashri.d.deshpande@kipi.bi",
            "rishabh.k.tomar@kipi.bi",
            "rahul.m.singh@kipi.bi",
            "utkarsh.a.kumar@kipi.bi",
            "arif.m.chaudhary@kipi.bi",
            "akshay.s.dwivedi@kipi.bi",
            "sumit.j.chahal@kipi.bi",
            "sunil.v.vuppala@kipi.bi",
            "anurag.a.supsande@kipi.bi",
            "vicky.d.deshpande@kipi.bi",
            "harshini.d.p@kipi.bi",
            "akash.s.sharma@kipi.bi",
            "madhulika.m.kumari@kipi.bi",
            "tanishq.h.chauhan@kipi.bi",
            "nishu.r.singh@kipi.bi",
            "sachin.g.gotal@kipi.bi",
            "sai.m.charan@kipi.bi"
    ]

    msg = MIMEMultipart()
    msg['Subject'] = "Hi Kipians! This is the first custom notification from Miniproject 2A Team 1"
    msg['From'] = fromaddr
    msg['To'] = ", ".join(toaddrs)

    #HTML Message Part
    html = """\
    <html>
    <body style = "background-image: url(https://images.unsplash.com/photo-1506318137071-a8e063b4bec0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=893&q=80);">
        <p><b><h1 style = "color: white ; text-align: center; ">
  Daily Reports By MetalBook </h1><h2  style = "color: white ;  text-align: right;">22-Feb-2022</h2></b>
        <br>
        <h2  style = "color: white ; padding-left: 40px;" >Snowflake Query Utilization</h2>
        <img src = 'cid:image0' style  =    "display: block;
  margin-left: auto;
  margin-right: auto;">
        <br>
        <h2  style = "color: white ; padding-left: 40px;" >Performance Monitoring</h2>
        <img src = 'cid:image1' style  =    "display: block;
  margin-left: auto;
  margin-right: auto;">
        <br>
        <h2  style = "color: white ; padding-left: 40px;" >Compute Cost Overview</h2>
        <img src = 'cid:image2' style  =    "display: block;
  margin-left: auto;
  margin-right: auto;">
        <br>
        <h2  style = "color: white ; padding-left: 40px;" >Slowest Running Queries</h2>
        <img src = 'cid:image3'style  =    "display: block;
  margin-left: auto;
  margin-right: auto;">
  <br>
        </p>
        </body>
    </html>"""
    part = MIMEText(html,"html")
    msg.attach(part)
    image = MIMEImage(open('./Snowflake Query Utilization.png', 'rb').read())
    image.add_header('Content-ID', '<image0>') 
    msg.attach(image)
    image1 = MIMEImage(open('./Performance Monitoring.png', 'rb').read())
    image1.add_header('Content-ID', '<image1>') 
    msg.attach(image1)
    image2 = MIMEImage(open('./Compute Cost Overview.png', 'rb').read())
    image2.add_header('Content-ID', '<image2>') 
    msg.attach(image2)
    image3 = MIMEImage(open('./Slowest Running Queries.png', 'rb').read())
    image3.add_header('Content-ID', '<image3>') 
    msg.attach(image3)

    # with tab_server.auth.sign_in(tableau_auth):
    #     tab_server.use_server_version()
    #     for i in range (0,len(viewID)):
    #         view = tab_server.views.get_by_id(viewID[i])
    #         variable = "cid:image"+str(i)
    #         text = MIMEText('<img src= '+variable +view.name +' ><br>', 'html')
    #         msg.attach(text)
    #         image = MIMEImage(open('./'+view.name+'.png', 'rb').read())
    #         image.add_header('Content-ID', '<image'+str(i)+'>')        # Define the image's ID as referenced in the HTML body above
    #         msg.attach(image)
    
    server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    server.set_debuglevel(1)
    server.login(fromaddr, os.environ.get('gmail_password'))
    server.sendmail(fromaddr,toaddrs ,msg.as_string())
    server.quit()


    
    # os.remove("./Snowflake Query Utilization.png")



# schedule.every().day.at("14.48").do(task)
# schedule.every(5).seconds.do(task)  

# while True:
#     schedule.run_pending()
#     time.sleep(1)
task()