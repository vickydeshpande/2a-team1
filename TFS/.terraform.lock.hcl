# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/chanzuckerberg/snowflake" {
  version = "0.25.36"
  hashes = [
    "h1:BmSGSVjmPFleRtd1bQp/C6XLST6R8GRVLvGGvosrU8w=",
    "zh:105d31f29b929d2930fdf81e660bbf5c2912295f781afad3aed73fa70b9e0f3a",
    "zh:1a4beac3acff583ea91b43f750fdc05db4559b0360d40617e6299845185597ff",
    "zh:2b583b54372ccff85df0ad8408381ea2a18999fb118b52f9b80cf04a7e24cb1c",
    "zh:302a8fd5b00f525aaf4c1a8d911ef4633cc320f474cb73fbc25278908357eaad",
    "zh:3dbf1373b0763015d00a1ce3d8c2ad95e85013c99c90038edd64c53a868145c7",
    "zh:52967c9b357dedd76afe006fb75fa3c65466c33baea03228df2c3b21ba75b958",
    "zh:61a5caf3589d54c515e4858d15c9aeab776a21a9ec786d93a103a2a1f57480b2",
    "zh:6bc1104f7497badd6904c45e73048f66243bdbb6437f9aae75d2ca4ecb9b66f4",
    "zh:724d9832cc3f1b3be1e6a05fc6f7171d4e98c12fb02ff1d6a9337997289ccb6f",
    "zh:a14ea23f41e098dfca2216e87ff8aed1a3e149badee2c04e163c04d0cb030035",
    "zh:c65967de85c67d986610badce480fe3b2629e4b648c09d6fc8c6363628d3a652",
    "zh:c98c93a4a69591ad0c1bddcfbab3c627190d7ad93f5b9660c281242bfb929bac",
    "zh:d4df6298ca201d95e0391c5b69f3d6e9cc6b3606971fccdddd132ad744a079cd",
    "zh:dab54ef9678f393c04d51001cd6d0ed8363f26e053ab6a432a810306e370cbea",
    "zh:ea73133ea1d7900210a4f929c2ae4bc79db04c2f77e2e478714927d00421a508",
    "zh:f194f358f767714d83841dd59bf0cfc09d824c9376d0402311a7db4dd871a811",
    "zh:f92f78440e2cb3b47e847b5551e8093966d019961f5622ea3c967582dbe9dda5",
  ]
}
