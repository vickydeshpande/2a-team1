locals {
  user_input = yamldecode(file("user_input.yml"))

  roles = tolist(local.user_input.roles)

  rbac = local.user_input.rbac

  ac_privs = local.user_input.ac_privs

  wh_privs = local.user_input.wh_privs

  db_privs = local.user_input.db_privs

  sh_privs = local.user_input.sh_privs

  sh_future_privs = local.user_input.sh_future_privs

  tb_privs = local.user_input.tb_privs

  tb_future_privs = local.user_input.tb_future_privs
  rbac_maps = {
    for rmap in local.rbac : "${rmap.grant}_to_${rmap.grantee}" => {
      "grant"   = rmap.grant
      "grantee" = rmap.grantee
    }
  }
  ac_privs_maps = {
    for ac_pv_map in local.ac_privs : "${ac_pv_map.role}_to_${ac_pv_map.priv}" => {
      "role" = ac_pv_map.role
      "priv" = ac_pv_map.priv
    }
  }

  wh_privs_maps = {
    for wh_pv_map in local.wh_privs : "${wh_pv_map.wh_name}_to_${wh_pv_map.priv}_to_${wh_pv_map.role}" => {
      "wh_name" = wh_pv_map.wh_name
      "role"    = wh_pv_map.role
      "priv"    = wh_pv_map.priv
    }
  }

  db_privs_maps = {
    for db_pv_map in local.db_privs : "${db_pv_map.db_name}_to_${db_pv_map.priv}_to_${db_pv_map.role}" => {
      "db_name" = db_pv_map.db_name
      "role"    = db_pv_map.role
      "priv"    = db_pv_map.priv
    }
  }
  sh_privs_maps = {
    for sh_pv_map in local.sh_privs : "${sh_pv_map.db_name}_to_${sh_pv_map.schema_name}_to_${sh_pv_map.priv}_to_${sh_pv_map.role}" => {
      "db_name"     = sh_pv_map.db_name
      "schema_name" = sh_pv_map.schema_name
      "role"        = sh_pv_map.role
      "priv"        = sh_pv_map.priv
    }
  }
  sh_future_privs_maps = {
    for sh_future_pv_map in local.sh_future_privs : "${sh_future_pv_map.db_name}_to_${sh_future_pv_map.priv}_to_${sh_future_pv_map.role}" => {
      "db_name" = sh_future_pv_map.db_name
      "role"    = sh_future_pv_map.role
      "priv"    = sh_future_pv_map.priv
    }
  }

  tb_privs_maps = {
    for tb_pv_map in local.tb_privs : "${tb_pv_map.db_name}_to_${tb_pv_map.schema_name}_to_${tb_pv_map.table_name}_to_${tb_pv_map.priv}_to_${tb_pv_map.role}" => {
      "db_name"     = tb_pv_map.db_name
      "schema_name" = tb_pv_map.schema_name
      "table_name"  = tb_pv_map.table_name
      "role"        = tb_pv_map.role
      "priv"        = tb_pv_map.priv
    }
  }
  tb_future_privs_maps = {
    for tb_future_pv_map in local.tb_future_privs : "${tb_future_pv_map.db_name}_to_${tb_future_pv_map.priv}_to_${tb_future_pv_map.role}" => {
      "db_name" = tb_future_pv_map.db_name
      "role"    = tb_future_pv_map.role
      "priv"    = tb_future_pv_map.priv
    }
  }
}


  