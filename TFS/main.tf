resource "snowflake_role" "new_role" {
  provider = snowflake.securityadmin
  for_each = toset(local.roles)
  name     = each.key
}

resource "snowflake_role_grants" "new_rbac" {
  provider   = snowflake.securityadmin
  for_each   = local.rbac_maps
  role_name  = each.value.grant
  roles      = [each.value.grantee]
  depends_on = [snowflake_role.new_role]
}
resource "snowflake_account_grant" "ac_grant" {
  provider          = snowflake.securityadmin
  for_each          = local.ac_privs_maps
  roles             = [each.value.role]
  privilege         = each.value.priv
  with_grant_option = false
  depends_on        = [snowflake_role.new_role]
}
resource "snowflake_warehouse_grant" "wh_grant" {
  provider          = snowflake.securityadmin
  for_each          = local.wh_privs_maps
  warehouse_name    = each.value.wh_name
  privilege         = each.value.priv
  roles             = [each.value.role]
  with_grant_option = false
  depends_on        = [snowflake_role.new_role]
}

resource "snowflake_database_grant" "db_grant" {
  provider          = snowflake.securityadmin
  for_each          = local.db_privs_maps
  database_name     = each.value.db_name
  privilege         = each.value.priv
  roles             = [each.value.role]
  with_grant_option = false
  depends_on        = [snowflake_role.new_role]
}

resource "snowflake_schema_grant" "sh_grant" {
  provider          = snowflake.securityadmin
  for_each          = local.sh_privs_maps
  database_name     = each.value.db_name
  schema_name       = each.value.schema_name
  privilege         = each.value.priv
  roles             = [each.value.role]
  with_grant_option = false
  depends_on        = [snowflake_role.new_role]
}

resource "snowflake_schema_grant" "sh_future_grant" {
  provider          = snowflake.securityadmin
  for_each          = local.sh_future_privs_maps
  database_name     = each.value.db_name
  privilege         = each.value.priv
  roles             = [each.value.role]
  on_future         = true
  with_grant_option = false
  depends_on        = [snowflake_role.new_role]
}

resource "snowflake_table_grant" "tb_grant" {
  provider          = snowflake.securityadmin
  for_each          = local.tb_privs_maps
  database_name     = each.value.db_name
  schema_name       = each.value.schema_name
  table_name        = each.value.table_name
  privilege         = each.value.priv
  roles             = [each.value.role]
  with_grant_option = false
  depends_on        = [snowflake_role.new_role]
}

resource "snowflake_table_grant" "tb_future_grant" {
  provider          = snowflake.securityadmin
  for_each          = local.tb_privs_maps
  database_name     = each.value.db_name
  privilege         = each.value.priv
  roles             = [each.value.role]
  on_future         = true
  with_grant_option = false
  depends_on        = [snowflake_role.new_role]
}

resource "snowflake_stream_grant" "stream_grant" {
  database_name = "METAL_BOOK"
  schema_name   = "STAGING"
  stream_name   = "KAFKA_STREAM"
  privilege     = "select"
  roles         = ["IM_DEV"]
  on_future     = false
  depends_on    = [snowflake_role.new_role]
}

resource "snowflake_task_grant" "task_grant" {
  database_name     = "METAL_BOOK"
  schema_name       = "STAGING"
  task_name         = "KAFKA_TASK_NEW"
  privilege         = "operate"
  roles             = ["IM_DEV"]
  on_future         = false
  with_grant_option = false
  depends_on        = [snowflake_role.new_role]
}
resource "snowflake_view_grant" "view_grant1" {
  database_name     = "METAL_BOOK"
  schema_name       = "ANALYTICS"
  view_name         = "MANUFACTURER_LIFECYCLE"
  privilege         = "select"
  roles             = ["DEV_ANALYST"]
  with_grant_option = true
  depends_on        = [snowflake_role.new_role]
}
resource "snowflake_view_grant" "view_grant2" {
  database_name     = "METAL_BOOK"
  schema_name       = "ANALYTICS"
  view_name         = "SUPPLIER_QC"
  privilege         = "select"
  roles             = ["DEV_ANALYST"]
  with_grant_option = true
  depends_on        = [snowflake_role.new_role]
}
