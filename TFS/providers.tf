terraform {
  required_providers {
    snowflake = {
      source = "chanzuckerberg/snowflake"
    }
  }
}

provider "snowflake" {
  account  = var.snowflake_account
  username = var.snowflake_username
  password = var.snowflake_password
}


provider "snowflake" {
  alias    = "sysadmin"
  account  = var.snowflake_account
  username = var.snowflake_username
  password = var.snowflake_password
  role     = "SYSADMIN"
}

provider "snowflake" {
  alias    = "securityadmin"
  account  = var.snowflake_account
  username = var.snowflake_username
  password = var.snowflake_password
  role     = "SECURITYADMIN"
}
