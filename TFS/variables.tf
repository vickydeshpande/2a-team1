variable "snowflake_account" {
  type        = string
  description = "Snowflake account name to connect to, along with the region to which the account belongs"
  default     = ""
}
variable "snowflake_username" {
  type        = string
  description = "Username to be used to connect to the specified Snowflake account"
  default     = ""
}
variable "snowflake_password" {
  type        = string
  description = "Password of the USername being used to connect to the specified Snowflake account"
  default     = ""
}