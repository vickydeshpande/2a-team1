import argparse
import logging
import getpass
import sys
import tableauserverclient as TSC


def create_project(server, project_item):
    try:
        project_item = server.projects.create(project_item)
        print('Created a new project called: %s' % project_item.name)
        return project_item
    except TSC.ServerResponseError:
        print('We have already created this project: %s' % project_item.name)
        sys.exit(1)

def create_childproject(server,child_item):
    try:
        child_item = server.projects.create(child_item)
        print('Created a new child project called: %s' % child_item.name)
        return child_item
    except TSC.ServerResponseError:
        print('We have already created this child project: %s' % child_item.name)
        sys.exit(1)


parser = argparse.ArgumentParser(description='Create new projects.')
parser.add_argument('--site', '-s', required=True ,help='site name')
parser.add_argument('--username','-u' ,required=True, help='username of user')
parser.add_argument('--project','-p',help='project name')
parser.add_argument('--group','-g',action='extend' ,nargs='+',help='Add group to you site , Can pass more then one group' )
parser.add_argument('--childproject','-c',action='extend' ,nargs='+' ,help='child project name')
parser.add_argument('--logging-level', '-l', choices=['debug', 'info', 'error'], default='error',
                        help='desired logging level (set to error by default)')


args = parser.parse_args()

password = getpass.getpass("Password: ")

logging_level = getattr(logging, args.logging_level.upper())
logging.basicConfig(level=logging_level)

tableau_auth = TSC.TableauAuth(args.username , password , args.site)
server = TSC.Server('https://prod-apnortheast-a.online.tableau.com/')

with server.auth.sign_in(tableau_auth):
        server.use_server_version()
        
        if(args.project):
            top_level_project = TSC.ProjectItem(name=args.project)
            top_level_project = create_project(server, top_level_project)


            if(args.childproject):
                for i in range (0,len(args.childproject)):
                    child_project = TSC.ProjectItem(name=args.childproject[i], parent_id=top_level_project.id)
                    child_project = create_childproject(server, child_project)
        
        if(args.group):
            for i in range (0,len(args.group)):
                group = TSC.GroupItem(args.group[i])
                group = server.groups.create(group)
                print("%s -- Group is Created " %args.group[i])
        
